# CodeOff

This is my attempt at the Resource Guru "CodeOff" challenge.

## Usage

The generation of encoded output can be utilised within the command line via the `mix code_off.translate` mix task.

You can call this mix task with either the `text` or `path` switches, e.g.

``` bash
$ mix code_off.translate --text "Hello
I am in trouble"
# 4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1
```

OR

``` bash
$ mix code_off.translate --path test/fixtures/input.sample
# 4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1
```

## Getting up and running

### Erlang and Elixir version
Versions are managed with `asdf` which can be installed with:
`git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.3.0`

#### For Ubuntu or other linux distros
``` bash
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bashrc
echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc
```
#### OR for Mac OSX
``` bash
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bash_profile
echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bash_profile
```
You'll need to add the erlang and elxir plugin with:
```
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
```

The version of erlang and exlir the project uses is managed with the `.tools-version` file, it's pretty self explanitary.

To use the correct version run: `asdf install`.

> The erlang and exlir installs are "local" to the project, so that we can use and update the elixir/erlang versions on a 'per project'' basis.

Full docs on asdf are [here](https://github.com/asdf-vm/asdf).
