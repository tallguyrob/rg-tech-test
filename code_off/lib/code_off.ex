defmodule CodeOff do
  alias CodeOff.{Obfuscator, Translator}

  defdelegate obfuscate(input), to: Obfuscator
  defdelegate translate(input), to: Translator
end
