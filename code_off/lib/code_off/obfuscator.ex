defmodule CodeOff.Obfuscator do
  @ascii_starting_position 64

  @boundaries ["|", "/", "\n"]

  def obfuscate(input) do
    try do
      {:ok, obfuscate!(input)}
    rescue
      _ -> {:error, "Unable to obfuscate provided input: #{input}"}
    end
  end

  def obfuscate!(input), do: do_obfuscate(input, 0)

  defp do_obfuscate("", _result), do: ""
  defp do_obfuscate(".", result), do: to_string(result + 1)
  defp do_obfuscate("-", result), do: char_at(result + 1)

  defp do_obfuscate(point, _) when point in @boundaries, do: point
  defp do_obfuscate(<<point::bytes-size(1)>>, _),        do: raise "Unable to obfuscate: #{point}"

  # When the following morse-code point is the same as the current
  defp do_obfuscate(<<point::bytes-size(1), point::bytes-size(1)>> <> rest, result) do
    do_obfuscate(point <> rest, result + 1)
  end
  # When the following morse-code point is not the same as the current
  defp do_obfuscate(<<point::bytes-size(1)>> <> rest, result) do
    do_obfuscate(point, result) <> do_obfuscate(rest, 0)
  end

  defp char_at(position), do: List.to_string([@ascii_starting_position + position])
end
