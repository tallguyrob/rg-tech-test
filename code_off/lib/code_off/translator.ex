defmodule CodeOff.Translator do

  @mappings %{
    "A" => ".-",
    "B" => "-...",
    "C" => "-.-.",
    "D" => "-..",
    "E" => ".",
    "F" => "..-.",
    "G" => "--.",
    "H" => "....",
    "I" => "..",
    "J" => ".---",
    "K" => "-.-",
    "L" => ".-..",
    "M" => "--",
    "N" => "-.",
    "O" => "---",
    "P" => ".--.",
    "Q" => "--.-",
    "R" => ".-.",
    "S" => "...",
    "T" => "-",
    "U" => "..-",
    "V" => "...-",
    "W" => ".--",
    "X" => "-..-",
    "Y" => "-.--",
    "Z" => "--..",
    "0" => "-----",
    "1" => ".----",
    "2" => "..---",
    "3" => "...--",
    "4" => "....-",
    "5" => ".....",
    "6" => "-....",
    "7" => "--...",
    "8" => "---..",
    "9" => "----.",
    "." => ".-.-.-",
    "," => "--..--",
  }

  def translate(input) do
    try do
      {:ok, translate!(input)}
    rescue
      _ -> {:error, "Unable to translate provided input: #{input}"}
    end
  end

  def translate!(input) do
    input
    |> String.upcase()
    |> String.split("\n")
    |> Enum.map(&translate_line/1)
    |> Enum.join("\n")
  end

  defp translate_line(line) do
    line
    |> String.split(~r/\s+/)
    |> Enum.map(&translate_word/1)
    |> Enum.join("/")
  end

  defp translate_word(word) do
    word
    |> String.codepoints()
    |> Enum.map(&translate_char/1)
    |> Enum.join("|")
  end

  defp translate_char(char_string), do: Map.fetch!(@mappings, char_string)
end
