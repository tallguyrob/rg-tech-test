defmodule Mix.Tasks.CodeOff.Translate do

  use Mix.Task

  @switches [path: :string, text: :string]

  @shortdoc "Generate encoded text from provided input"
  def run(args) do
    with {:ok, {source_type, source}} <- parse(args),
         {:ok, input}                 <- fetch_input(source_type, source),
         {:ok, translated}            <- CodeOff.translate(input),
         {:ok, encoded}               <- CodeOff.obfuscate(translated)
    do
      IO.write(encoded)
    else
      {:error, :enoent} -> Mix.raise("Could not find file for given path")
      {:error, err}     -> Mix.raise(err)
    end
  end

  defp fetch_input(:text, input), do: {:ok, input}
  defp fetch_input(:path, path),  do: File.read(path)

  defp parse(args) do
    {input_source, _, _} = OptionParser.parse(args, switches: @switches)

    case input_source do
      [input]  -> {:ok, input}
      []       -> {:error, "mix code_off.translate expects either the --text or --path switch"}
      _sources -> {:error, "mix code_off.translate expects either the --text or --path switch, not both"}
    end
  end
end
