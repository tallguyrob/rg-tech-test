defmodule CodeOff.TranslatorTest do
  alias CodeOff.Translator

  use ExUnit.Case, async: true

  @trouble_morse_code "-|.-.|---|..-|-...|.-..|."
  @sample_output "../.-|--/..|-./-|.-.|---|..-|-...|.-..|."

  describe ".translate/1" do
    @input "TROUBLE"
    @output @trouble_morse_code
    test "translates alphanumberic characters" do
      assert Translator.translate(@input) == {:ok, @output}
    end

    @input "trouble"
    @output @trouble_morse_code
    test "translates lowercase characters" do
      assert Translator.translate(@input) == {:ok, @output}
    end

    @input "I AM IN TROUBLE"
    @output @sample_output
    test "leaves whitespace intact" do
      assert Translator.translate(@input) == {:ok, @output}
    end

    @input "~*()"
    test "errors gracefully on bad input" do
      assert Translator.translate(@input) == {:error, "Unable to translate provided input: ~*()"}
    end

    @input "HELLO\nI AM IN TROUBLE"
    @output "....|.|.-..|.-..|---\n../.-|--/..|-./-|.-.|---|..-|-...|.-..|."
    test "translates multi-line input" do
      assert Translator.translate(@input) == {:ok, @output}
    end
  end
end
