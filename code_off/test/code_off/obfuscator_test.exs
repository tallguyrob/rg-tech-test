defmodule CodeOff.ObfuscatorTest do
  alias CodeOff.Obfuscator

  use ExUnit.Case, async: true

  @input ""
  @output ""
  test "handle empty string" do
    assert Obfuscator.obfuscate(@input) == {:ok, @output}
  end

  @input "..."
  @output "3"
  test "handle single characters only consisting of dots" do
    assert Obfuscator.obfuscate(@input) == {:ok, @output}
  end

  @input "---"
  @output "C"
  test "handle single characters only consisting of dashes" do
    assert Obfuscator.obfuscate(@input) == {:ok, @output}
  end

  @input "..--"
  @output "2B"
  test "handles a single character consisting of both dots and dashes" do
    assert Obfuscator.obfuscate(@input) == {:ok, @output}
  end

  @input "..--|...|.-.."
  @output "2B|3|1A2"
  test "handles multiple characters" do
    assert Obfuscator.obfuscate(@input) == {:ok, @output}
  end

  @input "..--|...|.-../--..|.-.."
  @output "2B|3|1A2/B2|1A2"
  test "handles multiple words" do
    assert Obfuscator.obfuscate(@input) == {:ok, @output}
  end

  @input "....|.|.-..|.-..|---\n../.-|--/..|-./-|.-.|---|..-|-...|.-..|."
  @output "4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1"
  test "handles multiple lines" do
    assert Obfuscator.obfuscate(@input) == {:ok, @output}
  end

  @input "cheese"
  test "gracefully errors on strange input" do
    assert Obfuscator.obfuscate(@input) == {:error, "Unable to obfuscate provided input: cheese"}
  end
end
