defmodule Mix.Tasks.CodeOff.TranslateTest do
  use ExUnit.Case, async: true

  import ExUnit.CaptureIO

  import Mix.Tasks.CodeOff.Translate

  test "handles missing input" do
    message = "mix code_off.translate expects either the --text or --path switch"
    assert_raise Mix.Error, message, fn -> run [] end
  end

  test "handles duplicate switches" do
    message = "mix code_off.translate expects either the --text or --path switch, not both"
    assert_raise Mix.Error, message, fn -> run ["--text", "foo", "--path", "bar"] end
  end

  test "generates a obfuscated morse-code when passed direct text" do
    output = capture_io(fn -> run(["--text", "hello"]) end)
    assert output == "4|1|1A2|1A2|C"
  end

  test "generates a obfuscated morse-code when passed a file path" do
    output = capture_io(fn -> run(["--path", "./test/fixtures/input.sample"]) end)
    assert output == "4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1\n"
  end
end
