ExUnit.start()

defmodule MyList do
  def flatten(list, inter \\ [])
  def flatten([], interim), do: interim
  def flatten([h | t], interim) when is_list(h) do
    flatten(h, flatten(t, interim))
  end
  def flatten([h | t], interim) do
    [h | flatten(t, interim)]
  end
end

defmodule MyListTest do
  use ExUnit.Case

  test "handles non_list inputs appropriately" do
    assert_raise FunctionClauseError, ~r/^no function clause matching/, fn ->
      MyList.flatten("sheep")
    end
  end

  @flattened [1, 2, 3, 4]

  @input [1, 2, 3, 4]
  test "handles pre-flattened lists" do
    assert MyList.flatten(@input) == @flattened
  end

  @input [1, [2], 3, 4]
  test "handles one-level nested lists lists" do
    assert MyList.flatten(@input) == @flattened
  end

  @input [1, [[2], 3], 4]
  test "handles two-level nested lists lists" do
    assert MyList.flatten(@input) == @flattened
  end

  @input [1, 2, [[]], 3, [4]]
  test "handles empty nested lists" do
    assert MyList.flatten(@input) == @flattened
  end

  @input [[], [], [[[]]]]
  test "handles nested empty lists" do
    assert MyList.flatten(@input) == []
  end
end
